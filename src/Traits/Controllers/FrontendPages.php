<?php

namespace wizpt\cms\Traits\Controllers;

use wizpt\cms\Models\Page;
use View;
use Illuminate\Support\Facades\Lang;

trait FrontendPages
{

    /**
     * Gets the page based on the slug
     *
     * @param      <string>  $slug   The slug
     *
     * @return     <type>  The page view or 404 if not found
     */
    public function getPage($slug) {

        $currLanguage = Lang::locale();
    	$page = Page::getActiveContentBySlug($slug,$currLanguage);
        if (empty($page)){
            return abort('404');
        }
        $data = [
            'page'=>$page
        ];
        return View('page', $data);
    }
}
