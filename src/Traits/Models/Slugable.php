<?php

namespace wizpt\cms\Traits\Models;

trait Slugable {

    /**
     * Gets the slug using the title 
     *
     * @param      string   $title       The title
     * @param      integer  $iteraction  The iteraction number
     *
     * @return     <string>   The slug.
     */
    public function getSlug($title, $iteraction = 1) {
        if ($iteraction == 1) {
            $sufix = '';
        } else {
            $sufix = '-' . $iteraction;
        }
        $slug = str_slug($title . $sufix);
        $slugs = self::where('slug', $slug)->get();
        if ($slugs->isEmpty()) {
            return $slug;
        } else {
            return self::getSlug($title,  ++$iteraction);
        }
    }

    /**
     * Update the slug
     *
     * @param      <string>  $slug   The slug
     *
     * @return     <string>  ( Slug validated )
     */
    public function updateSlug($slug) {
        $slugs = self::where('slug', $slug)
                ->get();
        if ($slugs->count() == 1) {
            return $slug;
        } else {
            return $this->getSlug($slug);
        }
    }

    /**
     * Gets a resource by it's slug
     *
     * @param      <string>  $slug   The slug
     *
     * @return     <collection>  ( The resourse or 404 if don't exist )
     */
    public static function getActiveContentBySlug($slug, $langParam) {

        $lang = (empty($langParam)) ? Session::get('locale') : $langParam;
        $foundPage = self::where('slug', $slug)
                ->where('active', 1)
                ->where('lang', $lang)
                ->first();
        return $foundPage;
    }

}
