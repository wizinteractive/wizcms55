<?php

namespace wizpt\cms\Traits\Models;

use Session;
//use Illuminate\Support\Facades\Input;

trait Translatable {

    public $translating_id;

    /**
     * Gets the translation.
     *
     * @param      <string>  $lang   The language
     *
     * @return     <Collection>  The Translation.
     */
    private function getActiveTrans($lang) {

        $translated = $this->modelClass::where('active', 1)
                ->where('lang', $lang)
                ->where(function($query) {
                    $query->where('translate', $this->translating_id)
                    ->orWhere('id', $this->translating_id);
                })
                ->get();
        return $translated;
    }

    /**
     * Determines if it has language.
     *
     * @param      <string>   $lang   The language
     *
     * @return     boolean  True if has translated, False otherwise.
     */
    public function hasLang($lang) {
        $translated = $this->getTrans($lang);
        if ($translated->isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Gets the path.
     *
     * @param      <string>   $lang   The language
     *
     * @return     string  The path.
     */
    public function getPath($lang) {
        $translated = $this->getTrans($lang);

        if ($translated->isEmpty()) {
            return false;
        } else {
            return $translated[0]->slug . "?lang=" . $lang;
        }
    }

    public function changeLang($slug) {
        if (Input::get('lang')) {
            Session::put("locale", Input::get('lang'));
            return redirect("/" . $slug);
        }
        return false;
    }

    
    public function haveTranslated($lang, $id) {
    	$page = $this::where("translate",$id)
    		->where('lang',$lang)
    		->get();
    	if($page->isEmpty()){
    		return false;
    	} else {
    		return true;
    	}
    }

    public function getTranslatedId($lang, $id) {
    	$page = $this::where('translate',$id)
    		->where('lang', $lang)
    		->get();
    	if($page->isEmpty()){
    		return false;
    	} else {
    		return $page[0]->id;
    	}
    }
    
    /**
     * Gets all the content that do not translate anything (traslates = 0)
     * 
     * @return type
     * 
     */
    public function getOriginalContents($exclude = null){
        $pagesOriginal = $this::where('translate',0)->get();
        return $pagesOriginal;
    }
    

}
