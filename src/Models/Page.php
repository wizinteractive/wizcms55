<?php

namespace wizpt\cms\Models;

use Illuminate\Database\Eloquent\Model;
use wizpt\cms\Traits\Models\Translatable;
use wizpt\cms\Traits\Models\Slugable;

class Page extends Model
{
    use Slugable;
    use Translatable;
    
    public $slugable = true;
    public $translatable = true;
    
    
    

    public function getParent($parent_id) { 
        $parent = $this::where("id",$parent_id)->get();
        return $parent;
    }

    public function getChildren($id) {
        $children = $this::where("parent",$id)->get();
        return $children;
    }
}
